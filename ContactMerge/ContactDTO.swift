import Foundation
import Outlaw

private var numImagesPerGender = 6

struct ContactDTO {
    
    var account: String
    var businessEmail: String?
    var businessPhone: String?
    var businessMobile: String?
    var email: String?
    var firstName: String
    var fullName: String?
    var gender: String?
    var iD: String?
    var jobTitleDescription: String?
    var lastName: String?
    var middleName: String?
    var mobile: String?
    var notes: String?
    var phone: String?
    var pictureThumbnailUrl: String?
    
}

extension ContactDTO: Deserializable {
    
    init(object: Outlaw.Extractable) throws {
        //Mandatory
            account = try object.value(for: "Account")
            firstName = try object.value(for: "FirstName")
        //Non-mandatory
            gender =  object.value(for: "Gender")
            businessEmail =  object.value(for: "BusinessEmail")
            businessPhone =  object.value(for: "BusinessPhone")
            businessMobile =  object.value(for: "BusinessMobile")
            email =  object.value(for: "Email")
            fullName =  object.value(for: "FullName")
            iD =  object.value(for: "ID")
            jobTitleDescription = object.value(for: "JobTitleDescription")
            lastName = object.value(for: "LastName")
            middleName = object.value(for: "MiddleName")
            mobile = object.value(for: "Mobile")
            notes = object.value(for: "Notes")
            phone = object.value(for: "Phone")
            pictureThumbnailUrl = object.value(for: "PictureThumbnailUrl")
        
    }
    
}

extension ContactDTO: Serializable {
    func serialized() -> [String: Any] {
        return ["Account": account,
                "FirstName": firstName,
                "BusinessEmail": businessEmail ?? "",
                "BusinessPhone": businessPhone ?? "",
                "BusinessMobile": businessMobile ?? "",
                "Email": email ?? "",                
                "FullName": fullName ?? "",
                "Gender": gender ?? "",
                "ID": iD ?? "",
                "JobTitleDescription": jobTitleDescription ?? "",
                "LastName": lastName ?? "",
                "MiddleName": middleName ?? "",
                "Mobile": mobile ?? "",
                "Notes": notes ?? "",
                "Phone": phone ?? "",
                "PictureThumbnailUrl": pictureThumbnailUrl ?? ""]
    }
}
