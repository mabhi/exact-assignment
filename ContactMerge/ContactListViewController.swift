import UIKit
import Toaster
import Foundation
import RxCocoa
import RxSwift
import RxDataSources

class ContactListViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    fileprivate var bag = DisposeBag()
    fileprivate var presenter: ContactListPresenter!
    fileprivate var dataSource = RxTableViewSectionedReloadDataSource<ContactSection>()

    override func viewDidLoad() {
        super.viewDidLoad()
                
        presenter = ContactListPresenter()
        
        ContactCell.register(with: tableView)
        
        presenter.loadData { [weak self] source in
            self?.newDataReceived(from: source)
        }
        
        initDataSource()
        initTableView()
    }
    
    func newDataReceived(from source: Source) {
        Toast(text: "New Data from \(source)").show()
        tableView.reloadData()
    }
}



//MARK: Reaction Process
extension ContactListViewController {
    func initDataSource() {
        dataSource.configureCell = { _, tableView, indexPath, contact in
            let cell = ContactCell.dequeue(from: tableView, for: indexPath, with: contact)
            return cell
        }
        
        dataSource.titleForHeaderInSection = { ds, index in
            return ds.sectionModels[index].header
        }
    }
    
    func initTableView() {
        presenter.sections.asObservable()
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)
        
        tableView.rx.itemSelected.map { indexPath in
            return (indexPath, self.dataSource[indexPath])
            }.subscribe(onNext: { indexPath, contact in
                self.next(with: contact)
            }).disposed(by: bag)
        
        tableView.rx.setDelegate(self)
            .disposed(by: bag)
    }
    
}


//MARK: - UITableViewDelegate
extension ContactListViewController {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    func next(with contact: ContactDTO) {
        
        let detailPresenter = DetailPresenter(with: contact)
        let detailViewController = DetailViewController.fromStoryboard(with: detailPresenter)
        
        navigationController?.pushViewController(detailViewController, animated: true)
    }

}
