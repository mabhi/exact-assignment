import Foundation
import RxSwift
import RxDataSources

typealias BlockWithSource = (Source)->Void
typealias VoidBlock = ()->Void

struct ContactSection {
    var header: String
    var items: [Item]
}
extension ContactSection: SectionModelType {
    typealias Item = ContactDTO
    
    init(original: ContactSection, items: [Item]) {
        self = original
        self.items = items
    }
}


class ContactListPresenter {
    var sections = Variable<[ContactSection]>([])
    fileprivate var bag = DisposeBag()
    fileprivate var data = Variable<[ContactDTO]>([])
    fileprivate var modelLayer = ModelLayer()
    
    init() {
         setupObservers()
    }
}


//MARK: - Reactive Process
extension ContactListPresenter {
    func setupObservers() {
        data.asObservable().subscribe(onNext: { [weak self] newContacts in
            self?.updateNewSections(with: newContacts)
        }).disposed(by: bag)
    }
    
    func updateNewSections(with newContacts: [ContactDTO]) {
        func mainWork() {
            sections.value = filter(contacts: newContacts)
        }
        
        func filter(contacts: [ContactDTO]) -> [ContactSection] {
            let grouped = contacts.grouped(by: {$0.account})
            var contactSections = [ContactSection]()
            grouped.forEach{
                contactSections.append(ContactSection(header: $0.key, items: $0.value))
            }
            return contactSections
        }
        
        mainWork()
    }
}

//MARK: - Data Methods
extension ContactListPresenter {
    
    func loadData(finished: @escaping BlockWithSource) {
        modelLayer.loadData { [weak self] source, contacts in
            self?.data.value = contacts
            finished(source)
        }
    }
}




