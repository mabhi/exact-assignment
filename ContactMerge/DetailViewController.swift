import UIKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    fileprivate var presenter: DetailPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    func configure(with presenter: DetailPresenter) {
        self.presenter = presenter
    }
    
    func setupView() {
        profileImage.image = UIImage(named: presenter.imageName)
        nameLabel.text = presenter.name
        
    }
}

//MARK: - Helper Methods
extension DetailViewController {
    static func fromStoryboard(with presenter: DetailPresenter) -> DetailViewController {
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            vc.configure(with: presenter)
        
        return vc
    }
}

