import Foundation

class ContactTranslator {
    func translate(from contact: Contact?) -> ContactDTO? {
        guard let contact = contact else { return nil }
        return ContactDTO(account: contact.Account,
                          businessEmail: contact.BusinessEmail,
                          businessPhone: contact.BusinessPhone,
                          businessMobile: contact.BusinessMobile,
                          email: contact.Email,
                          firstName: contact.FirstName,
                          fullName: contact.FullName,
                          gender: contact.Gender,
                          iD: contact.ID,
                          jobTitleDescription: contact.JobTitleDescription,
                          lastName: contact.LastName,
                          middleName: contact.MiddleName,
                          mobile: contact.Mobile,
                          notes: contact.Notes,
                          phone: contact.Phone,
                          pictureThumbnailUrl: contact.PictureThumbnailUrl)
        
    }
    
    func translate(from dto: ContactDTO?, with context: CBLDatabase) -> Contact? {
        guard let dto = dto else { return nil }
        let newContact = Contact(forNewDocumentIn: context)

        newContact.Account = dto.account
        newContact.BusinessEmail = dto.businessEmail ?? ""
        newContact.BusinessPhone = dto.businessPhone ?? ""
        newContact.BusinessMobile = dto.businessMobile ?? ""
        newContact.Email = dto.email ?? ""
        newContact.FirstName = dto.firstName
        newContact.FullName = dto.fullName ?? ""
        newContact.Gender = dto.gender ?? ""
        newContact.ID = dto.iD ?? ""
        newContact.JobTitleDescription = dto.jobTitleDescription ?? ""
        newContact.LastName = dto.lastName ?? ""
        newContact.MiddleName = dto.middleName ?? ""
        newContact.Mobile = dto.mobile ?? ""
        newContact.Notes = dto.notes ?? ""
        newContact.Phone = dto.phone ?? ""
        newContact.PictureThumbnailUrl = dto.pictureThumbnailUrl ?? ""
        
        
        return newContact
    }
}
