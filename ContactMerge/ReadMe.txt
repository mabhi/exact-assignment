Assignment:
Deliver a native mobile app which offers the user a delightful way of merging data from multiple contact persons into one record. It uses data from the Exact Online API.
•        Use case: A company can have duplicate data in their records. Multiple times the same contact persons (‘Contacts’) in a company they do business with (‘Account’). This can happen since multiple people might work with the ERP system. The app will offer a delightful way of combing data from multiple persons which are apparently the same, into one record. Retaining data potentially from both of them. One of the records might have the correct phone number, where the other record has the best picture. So you want to offer choice to the user of your app. Be smart about it.

Solution:

This application has been made with simplicity and as data assumptions has been made as understood.
For mock demo purpose, mock web server is being used, with json as was part of the original question. The JSON body has been modified slightly with 2 things:
1) JSON destructed one level up. So fmt now { 'result' : ....} instead of { 'd' : { 'results' : ...}}
2)Account Field values truncated to short names e.g. Company ...

The application flow is following MVP/MVVM pattern
Model Layer : consisting of Network layer / DataLayer and Translation Layer
View : Views and ViewControllers (e.g. Tables)
Presenter : For each View / ViewController
ViewModel : Observable data sequences subscribed by view controllers, react upon events from Model Layer
Database: Couchbase to support json documents and use map reduce to group and combine data.

Data duplication has been taken care by supposedy considering only first and fullnames and combining docs with same keys.

Shortcomings : Data dependency injection not included so classes are coupled to the extend of instantiating their dependencies instead of pushed, ideally.

Instructions:
1)Download from the git repo => https://mabhi@bitbucket.org/mabhi/exact-assignment.git
2)Install cocoapods if not present
3)Stay in the project folder and run `pod install`. Let the dependencies download. Then build and run the app.
