import Foundation

class ContactCellPresenter {
    var contact: ContactDTO
    
    var accountId: String { return contact.account }
    var imageName: String { return contact.pictureThumbnailUrl ?? ""}
    var name: String {
        let fname = contact.firstName
        let lname = contact.lastName ?? ""
        let fullName = contact.fullName ?? ""
        
        return "\(fname) - \(lname), \(fullName)"
    }
 
    init(with contact: ContactDTO) {
        self.contact = contact
    }
}

