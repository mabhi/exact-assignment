import Foundation

typealias ContactsAndSourceBlock = (Source, [ContactDTO])->Void

class ModelLayer {
    fileprivate var networkLayer = NetworkLayer();
    fileprivate var dataLayer = DataLayer();
    fileprivate var translationLayer = TranslationLayer();
    
    func loadData(resultsLoaded: @escaping ContactsAndSourceBlock) {
        func mainWork() {
            
            loadFromDB(from: .local)
            
            networkLayer.loadFromServer { data in
                if let dtos = self.translationLayer.createContactDTOsFromJsonData(data) {
                    self.dataLayer.save(dtos: dtos, translationLayer: self.translationLayer) {
                        loadFromDB(from: .network)
                    }
                }
            }
        }
        
        func loadFromDB(from source: Source) {
            print("loading from \(source.rawValue)")
            dataLayer.loadFromDB { contacts in
                let dtos = translationLayer.toContactDTOs(from: contacts);
                resultsLoaded(source, dtos)
            }
        }
        
        mainWork()
    }
    
    func loadCombinedData(resultsLoaded: @escaping ContactsAndSourceBlock) {
        func mainWork() {
            loadFromDB(from: .local)
        }
        
        func loadFromDB(from source: Source){
            print("loading from \(source.rawValue)")
            dataLayer.loadCombinedFromDB{ contactsData in
                guard let dtos = translationLayer.createContactDTOsFromJsonData(contactsData) else{
                    resultsLoaded(source, [])
                    return
                }
                resultsLoaded(source, dtos)
                
            }
        }
        mainWork()
    }
}
