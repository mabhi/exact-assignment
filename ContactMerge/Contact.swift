//
//  Contact.swift
//  ContactMerge
//
//  Created by Abhijit Mukherjee on 15/07/2017.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation

@objc(Contact)
class Contact: CBLModel {
    
    @NSManaged var Account: String
    @NSManaged var BusinessEmail: String
    @NSManaged var BusinessPhone: String
    @NSManaged var BusinessMobile: String
    @NSManaged var Email: String
    @NSManaged var FirstName: String
    @NSManaged var FullName: String
    @NSManaged var Gender: String
    @NSManaged var ID: String
    @NSManaged var JobTitleDescription: String
    @NSManaged var LastName: String
    @NSManaged var MiddleName: String
    @NSManaged var Mobile: String
    @NSManaged var Notes: String
    @NSManaged var Phone: String
    @NSManaged var PictureThumbnailUrl: String
    
}
 
