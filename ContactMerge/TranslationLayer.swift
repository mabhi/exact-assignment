import Foundation
import Outlaw

class TranslationLayer {
    
    fileprivate var contactTranslator = ContactTranslator()
    
    func createContactDTOsFromJsonData(_ data: Data) -> [ContactDTO]? {
        print("converting json to DTOs")
        
        do {
            let json:[String: Any] = try JSON.value(from: data)
            let contacts: [ContactDTO] = try json.value(for: "results")
            return contacts
        } catch {
            print("unexpected error during contact DTO create => \(error.localizedDescription)")
        }
        
        return nil
    }
    
    func toUnsavedCouchbaseData(from dtos: [ContactDTO], with context: CBLDatabase) -> [Contact] {
        print("convering DTOs to CBLModel Data Objects")
        let contacts = dtos.flatMap{ dto in contactTranslator.translate(from: dto, with: context) } // keeping it simple by keeping things single threaded
        
        return contacts
    }
    
    func toContactDTOs(from contacts:[Contact]) -> [ContactDTO] {
        let dtos = contacts.flatMap { contactTranslator.translate(from: $0) }
        
        return dtos
    }
}
