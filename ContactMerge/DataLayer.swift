//
//  DataLayer.swift
//  ContactMerge
//
//  Created by Abhijit Mukherjee on 15/07/2017.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation


typealias ContactsBlock = ([Contact])->Void
typealias RawDataBlock = (Data)->Void
class DataLayer : NSObject{
    
    
    func handleError(_ theError : NSError?){
        if let error = theError as NSError? {
            print("Unresolved error \(error), \(error.userInfo)")
        }
    }
    
    lazy var persistentContainer: CBLDatabase = {
        
        do{
            let manager = CBLManager.sharedInstance()
            let container = try manager.databaseNamed("my-database")
            return container
        } catch let error as NSError {
            fatalError("Cannot start a session: \(error)")
            
        }
    
    }()
    
    
    func save(dtos: [ContactDTO], translationLayer: TranslationLayer, finished: @escaping () -> Void) {
        clearOldResults()
        
        _ = translationLayer.toUnsavedCouchbaseData(from: dtos, with: persistentContainer)
        
        do {
            try persistentContainer.saveAllModels()
        } catch let error as NSError {
            handleError(error)
        }
        
        
        finished()
    }
    
    func loadFromDB(finished: ContactsBlock) {
        let contacts = loadContactsFromDB()
        print("loading data locally => \(contacts.count)")
        
        finished(contacts)
    }
    
    func loadCombinedFromDB(finished: RawDataBlock){
        let contactsData = loadCombinedContactsFromDB()
        finished(contactsData)
    }
}

//MARK: - setup queries
extension DataLayer{
    
    func setupViewAndQuery() -> CBLQuery{
        
        let listsView = persistentContainer.viewNamed("list/listsByAccount")
        
        if listsView.mapBlock == nil {
            listsView.setMapBlock({ (doc, emit) in
                if let type: String = doc["Account"] as? String
                {
                    if let firstNameKey =  doc["FirstName"] as? String,
                        let firstWord = firstNameKey.components(separatedBy: " ").first
                        {
                            
                        
                        if firstWord.characters.count > 0{
                            print("\(type) => Firstname \(firstWord)")
                            emit([type,firstWord],doc)
                            return

                        }

                    }
                    if let firstWordOfFullNameKey = doc["FullName"] as? String,
                        let firstWord = firstWordOfFullNameKey.components(separatedBy: " ").first
                    {
                        
//                        emit("\(type)#\(firstWord)",doc)
                        if firstWord.characters.count > 0{
                            print("\(type) => FullName \(firstWord)")
                            emit([type,firstWord],doc)
                            return

                        }
                    }
                        emit(type, doc)
                    
                    
                }
            }, reduce: { (keys, values, rereduce) -> Any in
                var result = Dictionary<String, String>()
                let _ = values.flatMap{
                    //Ignore the private keys (starting with _) and merge the documents if the value does not
                    //exist in the result map
                    if let doc = $0 as? Dictionary<String,Any>{
                        doc.forEach{ pair in
                            if let theValue = pair.value as? String,
                            !theValue.isEmpty && !pair.key.hasPrefix("_"){
                                result[pair.key] = theValue
                            }
                        }
                    }
                    
                }
                print("\(result)")

                return result
            }, version: "1.0")
        }
        
        
        return listsView.createQuery()
        
    }


}

//MARK: - Private Data Methods
extension DataLayer {
    
    // This will be used during initial setup or whenver all documents are asked for.
    // This is the configured for the simplest of cases, no conflicts only all docs
    
    fileprivate func loadContactsFromDB() -> [Contact] {
        
        let query = persistentContainer.createAllDocumentsQuery()
        return contactResults(from: query)
        
        
    }
    
    fileprivate func loadCombinedContactsFromDB() -> Data {
        
        let query = self.setupViewAndQuery()
        query.groupLevel = 2
        return rawContactResults(from: query)
    }
    
    //MARK: - Helper Methods
    
    private func rawContactResults(from query:(CBLQuery)) -> Data{
        var rawDataCollection = [Dictionary<String, String>]()
        let contactsData:Data
        do {
            let result = try query.run()
            while let row = result.nextRow() {
                if let mappedRow = row.value as? Dictionary<String, String>{
                    rawDataCollection.append(mappedRow)
                }
            }
            contactsData = try JSONSerialization.data(withJSONObject: ["results":rawDataCollection])
            
        }catch let error as NSError {
            handleError(error)
            //return empty results
            contactsData = try! JSONSerialization.data(withJSONObject: ["results":[]])
        }
        
        return contactsData
    }
    
    private func contactResults(from query:(CBLQuery)) -> [Contact]{
        var contacts = [Contact]()
        do {
            let result = try query.run()
            while let row = result.nextRow() {
                if let docId = row.documentID,
                    let doc = row.document,
                    let contact = Contact(for: doc)
                {
                    print("!!! Found in document \(docId)")
                    contacts.append(contact)
                }
            }
        } catch let error as NSError {
            handleError(error)
        }
        return contacts
    }
    
    fileprivate func clearOldResults() {
        print("clearing old results")
       
        let query = persistentContainer.createAllDocumentsQuery()
        do {
            let result = try query.run()
            while let row = result.nextRow() {
                if let docId = row.documentID,
                    let doc = row.document
                    {
                    print("!!! Found in document \(docId)")
                    try doc.purgeDocument()
                }
            }

        } catch let error as NSError {
            handleError(error)
        }
    }
}
/*
//MARK: - CBLDocument helper method
extension CBLDocument {
    
    func subDocumentWithNullsAndBlankValues() -> CBLJSON?{
        var dict = [String: Any]()
        guard let theTargetProperties = self.userProperties else{
            return nil
        }
        theTargetProperties.filter{ $0.value !== nu}
        
    }
}
 */
