import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameValueLabel: UILabel!
    @IBOutlet var companyLabel: UILabel!
    @IBOutlet var companyValueLabel: UILabel!
    @IBOutlet var imageContainer: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var presenter: ContactCellPresenter!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        commonInit()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageContainer.subviews.forEach { $0.removeFromSuperview() }
        nameValueLabel.text = ""
        companyValueLabel.text = ""
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: adding images
extension ContactCell {
    func add(imageName: String) {
        guard let image = UIImage(named: imageName) else { return }
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.image = image
        
        imageContainer.addSubview(imageView)
    }
}

//MARK: - Configure
extension ContactCell {
    func configure(with presenter: ContactCellPresenter) {
        
        getSomeData { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.set(company: presenter.accountId)
            strongSelf.set(name: presenter.name)
            strongSelf.add(imageName: presenter.imageName)
        }
    }
    
    fileprivate func getSomeData(finished: @escaping ()->Void ) {
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        //faking some network call
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.activityIndicator.isHidden = true
            
            finished()
        }
    }
}

//MARK: - Dynamic Sizing font - iOS 10
extension ContactCell {
    
    func commonInit() {
        
        setAccessibilityProperties()
        
        if #available(iOS 10, *) {
            commonInit_iOS10()
        } else {
            commonInit_iOS7plus()
        }
    }
    
    func setAccessibilityProperties() {
        nameValueLabel.isAccessibilityElement = true
    }
    
    func set(name: String) {
        nameValueLabel.text = name
        nameValueLabel.accessibilityValue = name
    }
    
    func set(company: String) {
        companyValueLabel.text  = company
    }
}

@available(iOS 10, *)
extension ContactCell {
    func commonInit_iOS10() {
        nameValueLabel.adjustsFontForContentSizeCategory = true
    }
}

//MARK: - Dynamic Sizing font - iOS 8
@available(iOS 7, *)
extension ContactCell {
    fileprivate func commonInit_iOS7plus() {
        assignFonts()
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
    }
    
    func userChangedTextSize(notification: NSNotification) {
        assignFonts()
    }
    
    fileprivate func assignFonts() {
        nameValueLabel.font = UIFont.preferredFont(forTextStyle: .body)
    }
}


//MARK: - Helper Methods
extension ContactCell {
    public static var cellId: String {
        return "ContactCell"
    }
    
    public static var bundle: Bundle {
        return Bundle(for: ContactCell.self)
    }
    
    public static var nib: UINib {
        return UINib(nibName: ContactCell.cellId, bundle: ContactCell.bundle)
    }
    
    public static func register(with tableView: UITableView) {
        tableView.register(ContactCell.nib, forCellReuseIdentifier: ContactCell.cellId)
    }
    
    public static func loadFromNib(owner: Any?) -> ContactCell {
        return bundle.loadNibNamed(ContactCell.cellId, owner: owner, options: nil)?.first as! ContactCell
    }
    
    public static func dequeue(from tableView: UITableView, for indexPath: IndexPath, with contact: ContactDTO) -> ContactCell {
        let contactCellPresenter = ContactCellPresenter(with: contact)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactCell.cellId, for: indexPath) as! ContactCell
        cell.configure(with: contactCellPresenter)
        return cell
    }
}
